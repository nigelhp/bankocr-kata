import Dependencies._

ThisBuild / scalaVersion     := "2.12.8"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.nigelhp"

lazy val root = (project in file(".")).
  configs(IntegrationTest).
  settings(
    name := "bankocr-kata",
    scalacOptions ++= Seq(
      "-target:jvm-1.8",
      "-encoding", "utf8",
      "-deprecation",
      "-feature",
      "-unchecked",
      "-Xcheckinit",
      "-Xlint:_",
      "-Xfatal-warnings",
      "-Yno-adapted-args",
      "-Ywarn-dead-code",
      "-Ywarn-value-discard",
      "-Ywarn-unused"
    ),
    scapegoatVersion in ThisBuild := "1.3.8",
    Defaults.itSettings,
    libraryDependencies ++= Seq(
      scalaCheck % Test,
      scalaTest % s"$Test,$IntegrationTest"
    )
  )
