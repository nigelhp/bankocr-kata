package com.nigelhp.bank.ut.support


import com.nigelhp.bank.model.AccountNumber

object ScanEntry {
  def ofAll(values: Seq[AccountNumber]): Seq[String] =
    values.flatMap(of)

  def of(accountNumber: AccountNumber): List[String] = {
    val scanLines = accountNumber.toList.flatMap(Digits.charToDigit).transpose
    scanLines.map(_.reduceLeft(_ + _))
  }

  private object Digits {
    private val Zero = Seq(
      " _ ",
      "| |",
      "|_|"
    )

    private val One = Seq(
      "   ",
      "  |",
      "  |"
    )

    private val Two = Seq(
      " _ ",
      " _|",
      "|_ "
    )

    private val Three = Seq(
      " _ ",
      " _|",
      " _|"
    )

    private val Four = Seq(
      "   ",
      "|_|",
      "  |"
    )

    private val Five = Seq(
      " _ ",
      "|_ ",
      " _|"
    )

    private val Six = Seq(
      " _ ",
      "|_ ",
      "|_|"
    )

    private val Seven = Seq(
      " _ ",
      "  |",
      "  |"
    )

    private val Eight = Seq(
      " _ ",
      "|_|",
      "|_|"
    )

    private val Nine = Seq(
      " _ ",
      "|_|",
      " _|"
    )

    private val Spacer = "   "

    type Digit = Seq[String]

    def charToDigit(char: Char): Option[Digit] =
      numCharToDigit.lift(char).map(_ :+ Spacer)

    private def numCharToDigit: PartialFunction[Char, Digit] = {
      case '0' => Zero
      case '1' => One
      case '2' => Two
      case '3' => Three
      case '4' => Four
      case '5' => Five
      case '6' => Six
      case '7' => Seven
      case '8' => Eight
      case '9' => Nine
    }
  }
}