package com.nigelhp.bank.ut.support


import com.nigelhp.bank.model.AccountNumber
import org.scalacheck.Gen

object GenAccountNumber {
  def genAccountNumber(withLength: Int): Gen[AccountNumber] =
    genAccountChars(withLength).map(toAccountNumber)

  private def genAccountChars(n: Int): Gen[List[Char]] =
    Gen.listOfN(n, Gen.numChar)

  private def toAccountNumber(accountChars: List[Char]): AccountNumber =
    accountChars.mkString

  // provides a suitable 'whenever ...' predicate to accompany Gen[AccountNumber]
  def isAccountNumber(withLength: Int)(accountNumber: AccountNumber): Boolean =
    accountNumber.length == withLength && accountNumber.forall(_.isDigit)
}
