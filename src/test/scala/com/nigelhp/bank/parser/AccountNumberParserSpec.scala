package com.nigelhp.bank.parser


import com.nigelhp.bank.model.AccountNumber
import com.nigelhp.bank.parser.AccountNumberParserSpec.parse
import org.scalatest.{FreeSpec, Matchers}

class AccountNumberParserSpec extends FreeSpec with Matchers {

  "An AccountNumber Parser" - {
    "can parse the digit 1" in {
      val digit = Seq(
        "   ",
        "  |",
        "  |",
        "   "
      )

      parse(digit) should contain theSameElementsAs Seq("1")
    }

    "can parse the digit 2" in {
      val digit = Seq(
        " _ ",
        " _|",
        "|_ ",
        "   "
      )

      parse(digit) should contain theSameElementsAs Seq("2")
    }

    "can parse the digit 3" in {
      val digit = Seq(
        " _ ",
        " _|",
        " _|",
        "   "
      )

      parse(digit) should contain theSameElementsAs Seq("3")
    }

    "can parse the digit 4" in {
      val digit = Seq(
        "   ",
        "|_|",
        "  |",
        "   "
      )

      parse(digit) should contain theSameElementsAs Seq("4")
    }

    "can parse the digit 5" in {
      val digit = Seq(
        " _ ",
        "|_ ",
        " _|",
        "   "
      )

      parse(digit) should contain theSameElementsAs Seq("5")
    }

    "can parse the digit 6" in {
      val digit = Seq(
        " _ ",
        "|_ ",
        "|_|",
        "   "
      )

      parse(digit) should contain theSameElementsAs Seq("6")
    }

    "can parse the digit 7" in {
      val digit = Seq(
        " _ ",
        "  |",
        "  |",
        "   "
      )

      parse(digit) should contain theSameElementsAs Seq("7")
    }

    "can parse the digit 8" in {
      val digit = Seq(
        " _ ",
        "|_|",
        "|_|",
        "   "
      )

      parse(digit) should contain theSameElementsAs Seq("8")
    }

    "can parse the digit 9" in {
      val digit = Seq(
        " _ ",
        "|_|",
        " _|",
        "   "
      )

      parse(digit) should contain theSameElementsAs Seq("9")
    }

    "can parse the digit 0" in {
      val digit = Seq(
        " _ ",
        "| |",
        "|_|",
        "   "
      )

      parse(digit) should contain theSameElementsAs Seq("0")
    }

    "can parse the account number 1234567890" in {
      val accountNumber = Seq(
        "    _  _     _  _  _  _  _  _ ",
        "  | _| _||_||_ |_   ||_||_|| |",
        "  ||_  _|  | _||_|  ||_| _||_|",
        "                              "
      )

      parse(accountNumber) should contain theSameElementsInOrderAs Seq("1234567890")
    }
  }
}

private object AccountNumberParserSpec {
  def parse(input: Seq[String]): Stream[AccountNumber] =
    AccountNumberParser.parse(input.toStream)
}