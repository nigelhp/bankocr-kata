package com.nigelhp.bank.parser


import com.nigelhp.bank.model.AccountNumber
import com.nigelhp.bank.parser.AccountNumberParserPropertySpec.{allAccountNumbersAreValid, genAccountNumbers}
import com.nigelhp.bank.ut.support.{GenAccountNumber, ScanEntry}
import org.scalacheck.Gen
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{FreeSpec, Matchers}


class AccountNumberParserPropertySpec extends FreeSpec with Matchers with GeneratorDrivenPropertyChecks {

  implicit override val generatorDrivenConfig = PropertyCheckConfiguration(minSuccessful = 100, sizeRange = 500)

  "An AccountNumber Parser" - {
    "can parse a stream of valid scan entries" in {
      forAll(genAccountNumbers) { accountNumbers =>
        whenever(allAccountNumbersAreValid(accountNumbers)) {
          val scanEntries = ScanEntry.ofAll(accountNumbers).toStream

          AccountNumberParser.parse(scanEntries) should contain theSameElementsInOrderAs accountNumbers
        }
      }
    }
  }
}

private object AccountNumberParserPropertySpec {
  private val AccountNumberLength = 9

  val genAccountNumbers: Gen[List[AccountNumber]] =
    Gen.listOf(GenAccountNumber.genAccountNumber(withLength = AccountNumberLength))

  def allAccountNumbersAreValid(values: List[AccountNumber]): Boolean =
    values.forall(GenAccountNumber.isAccountNumber(withLength = AccountNumberLength))
}