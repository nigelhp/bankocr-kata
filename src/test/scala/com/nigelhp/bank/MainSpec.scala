package com.nigelhp.bank

import org.scalatest.{FreeSpec, Matchers}


class MainSpec extends FreeSpec with Matchers {

  "Main" - {
    "validates input arguments" - {
      "rejecting" - {
        "too few arguments" in {
          Main.validate(Array.empty) shouldBe None
        }
        
        "too many arguments" in {
          Main.validate(Array("one", "two")) shouldBe None
        }
      }
    }
  }
}
