package com.nigelhp.bank.parser

import com.nigelhp.bank.model.AccountNumber


object AccountNumberParser {
  private val LinesPerAccountNumber = 4
  private val CharsPerDigit = 3

  def parse(lines: Stream[String]): Stream[AccountNumber] =
    lines.grouped(LinesPerAccountNumber).map { accountLines =>
      toAccountNumber(accountLines.force.toList)
    }.toStream

  private def toAccountNumber(lines: Seq[String]): AccountNumber = {
    val linesOfDigitParts = lines.map(_.grouped(CharsPerDigit).toSeq)
    linesOfDigitParts.transpose.map(toDigit).mkString
  }

  private def toDigit(parts: Seq[String]): Char = {
    import Digits._
    parts match {
      case Zero => '0'
      case One => '1'
      case Two => '2'
      case Three => '3'
      case Four => '4'
      case Five => '5'
      case Six => '6'
      case Seven => '7'
      case Eight => '8'
      case Nine => '9'
      case _ => '?'
    }
  }

  private object Digits {
    private val Separator = "   "

    val Zero = makeSeq(
      " _ ",
      "| |",
      "|_|")

    val One = makeSeq(
      "   ",
      "  |",
      "  |")

    val Two = makeSeq(
      " _ ",
      " _|",
      "|_ ")

    val Three = makeSeq(
      " _ ",
      " _|",
      " _|")

    val Four = makeSeq(
      "   ",
      "|_|",
      "  |")

    val Five = makeSeq(
      " _ ",
      "|_ ",
      " _|")

    val Six = makeSeq(
      " _ ",
      "|_ ",
      "|_|")

    val Seven = makeSeq(
      " _ ",
      "  |",
      "  |")

    val Eight = makeSeq(
      " _ ",
      "|_|",
      "|_|")

    val Nine = makeSeq(
      " _ ",
      "|_|",
      " _|")

    private def makeSeq(first: String, mid: String, last: String): Seq[String] =
      Seq(first, mid, last, Separator)
  }
}
