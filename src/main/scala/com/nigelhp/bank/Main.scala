package com.nigelhp.bank


import com.nigelhp.bank.model.AccountNumber
import com.nigelhp.bank.parser.AccountNumberParser

import scala.io.Codec.UTF8
import scala.io.Source


object Main {
  def main(args: Array[String]): Unit =
    validate(args).fold(printUsage()) { scanOutPath =>
      parseEntries(scanOutPath).foreach(println)
    }

  def validate(args: Array[String]): Option[String] =
    if (args.length > 1) None
    else args.headOption

  private def printUsage(): Unit =
    println("Usage: Main /path/to/scanner-out")

  def parseEntries(scanOutPath: String): Stream[AccountNumber] = {
    val src = Source.fromFile(scanOutPath)(UTF8)
    val lines = src.getLines().toStream
    AccountNumberParser.parse(lines)
  }
}
