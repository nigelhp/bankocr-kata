package com.nigelhp.bank.it.support


import java.nio.file.{Files, Path}

import scala.collection.JavaConverters._

private [it] object ScannerOutputFile {
  private val FilePrefix = "bank"
  private val FileSuffix = ".ocr"

  def withAScannerOutputFileContaining[A](content: Seq[String])(f: Path => A): A = {
    val path = Files.createTempFile(FilePrefix, FileSuffix)
    try {
      Files.write(path, content.asJava)
      f(path)
    } finally {
      Files.delete(path)
    }
  }
}
