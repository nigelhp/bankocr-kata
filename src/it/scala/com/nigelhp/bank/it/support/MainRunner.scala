package com.nigelhp.bank.it.support


import java.io.ByteArrayOutputStream
import java.nio.file.Path

import com.nigelhp.bank.Main

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, blocking}
import scala.io.Source

private [it] object MainRunner {
  def readScannerFile(atPath: Path): Future[Seq[String]] =
    run(Main.main)(withArgs = atPath.toString).map(toLines)

  private def run(main: Array[String] => Unit)(withArgs: String*): Future[Source] =
    Future {
      val out = new ByteArrayOutputStream()
      blocking {
        Console.withOut(out) {
          main(withArgs.toArray)
        }
      }

      Source.fromBytes(out.toByteArray)
    }

  private def toLines(source: Source): Seq[String] =
    source.getLines().toSeq
}
