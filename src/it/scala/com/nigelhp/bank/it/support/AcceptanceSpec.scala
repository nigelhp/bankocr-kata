package com.nigelhp.bank.it.support


import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{FeatureSpec, GivenWhenThen, Matchers}

class AcceptanceSpec extends FeatureSpec with GivenWhenThen with Matchers with ScalaFutures
